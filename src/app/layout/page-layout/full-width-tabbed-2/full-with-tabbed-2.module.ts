import { NgModule, Input } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import {MatTabsModule} from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete'; 

import { CardedFullWidthTabbed2Component } from 'app/layout/page-layout/full-width-tabbed-2/full-width-tabbed-2.component';

@NgModule({
    declarations: [
        CardedFullWidthTabbed2Component,
    ],
    imports     : [
        RouterModule,
        CommonModule,
        MatTabsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatDatepickerModule,
        MatRadioModule,
        MatIconModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        
    ],
    exports     : [
        CardedFullWidthTabbed2Component
    ]
})
export class CardedFullWidthTabbed2Module
{
}
