import { Component, Input, TemplateRef, ContentChild } from '@angular/core';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
    selector   : 'carded-fullwidth-tabbed-2',
    templateUrl: './full-width-tabbed-2.component.html',
    styleUrls  : ['./full-width-tabbed-2.component.scss'],
    animations: [
        trigger('filtersExpand', [
            state('collapsed', style({height: '0px', minHeight: '0'})),
            state('expanded', style({height: '55vh', position: 'relative', zIndex:'5', overflowY:'scroll'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]
})
export class CardedFullWidthTabbed2Component
{
    @Input() categoryTitle: string;
    @ContentChild(TemplateRef, {static: true}) contentTemplate: TemplateRef<any>;

    isExpanded = false;

    contracts = [
        {type: 'Démo'},
        {type: 'Test'},
        {type: 'A l\'annonce'},
        {type: 'Crédits'},
        {type: 'Mensuel'},
        {type: 'Annuel'},
    ];
    payments = [
        {type: 'Prélèvement'},
        {type: 'Autre'},
    ];
    departementList = [35, 86, 93, 15];
    countryList = [
        'France',
        'Angleterre',
        'Espagne'
    ];
    csmList = [
        'Marine',
        'Lisa'
    ];
    salesList = [
        'Maëlle',
        'Mélanie'
    ];
    entrepriseSizeList = [
        '0 à 10 salariés',
        '11 à 21 salariés'
    ];

    defaultFunctionnality = 'Aucune';
    functionnalityControl = new FormControl();
    functionnalityList: string[] = [
        // Can't seem to set default value of the multiple select, weird...
        // 'Aucune',
        'Abonnement We Recruit',
        'Bourse à l\'emploi',
        'Candidature par Email',
        'Création d\'entreprises',
        'Diffusions'
    ];

    sectorControl = new FormControl();
    sectorList: string[] = [
        'Industrie Manufacturière et Production',
        'Ingénierie - Bureau d\'études',
        'Recrutement - Conseil RH - Interim'
    ];
    sectorFilteredOptionList: Observable<string[]>;

    toggleIsExpanded() {
        this.isExpanded = !this.isExpanded;
        console.log(this.isExpanded)
    }

    /**
     * Constructor
     */
    constructor()
    {
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();

    return this.sectorList.filter(sector => sector.toLowerCase().indexOf(filterValue) === 0);
    }
    ngOnInit(){
        this.sectorFilteredOptionList = this.sectorControl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );
            
    }
}
