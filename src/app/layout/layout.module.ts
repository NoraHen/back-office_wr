import { NgModule } from '@angular/core';

import { VerticalLayout2Module } from 'app/layout/vertical/layout-2/layout-2.module';
import { CardedFullWidthTabbed2Module } from 'app/layout/page-layout/full-width-tabbed-2/full-with-tabbed-2.module';


@NgModule({
    imports: [
        VerticalLayout2Module,
        CardedFullWidthTabbed2Module

    ],
    exports: [
        VerticalLayout2Module,
        CardedFullWidthTabbed2Module

    ]
})
export class LayoutModule
{
}
