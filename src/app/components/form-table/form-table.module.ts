import { NgModule, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';

import { FormTableComponent } from './form-table.component';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
    declarations: [
    ],
    imports     : [
        CommonModule,
        FormTableComponent,
        MatTableModule,
        CdkTableModule,
        Input,
        MatTabsModule
        
    ],
    exports     : [
        FormTableComponent
    ]
})

export class FormTableModule
{
}
