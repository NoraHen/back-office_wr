import { Component, OnInit, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-table',
  templateUrl: './form-table.component.html',
  styleUrls: ['./form-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class FormTableComponent implements OnInit {

  @Input() columnsToDisplay: Array<string>;
  @Input() dataSource: Array<any>;
  @Input() holdingName: string;

  expandedElement;
  holdingClickCount = 0;
  
  csms = [
    {name: 'Marine'},
    {name: 'Lisa'},
  ]
  sales = [
    {name: 'Maëlle'},
    {name: 'Mélanie'},
  ]
  taillesEntreprise = [
    {size: 'Entreprise Size 1'},
    {size: 'Entreprise Size 2'},
    {size: 'Entreprise Size 3'}
  ]
  secteurs = [
    {name: 'Secteur Name 1'},
    {name: 'Secteur Name 2'},
    {name: 'Secteur Name 3'}
  ]
  contracts = [
    {type: 'Démo'},
    {type: 'Test'},
    {type: 'A l\'annonce'},
    {type: 'Crédits'},
    {type: 'Mensuel'},
    {type: 'Annuel'},
  ]
  payments = [
    {type: 'Prélèvement'},
    {type: 'Autre'},
  ]

  // Utilisateurs Component
  usersColumns = [
    'Nom',
    'Prénom',
    'Email',
    'Entité',
    'Rôle',
    'Statut',
    'Actions'
  ]
  users = [
    {
      'Nom': 'Goux',
      'Prénom': 'Pierre',
      'Email': 'goux.pierre@mail.com',
      // Maybe a different logo for holding and entreprise?
      'Entité': 'Dev Insight - Holding',
      'Rôle': 'Administrator',
      'Statut': 'active',
      'Actions': ['edit', 'delete', 'connection']
    },
    {
      'Nom': 'Gaucher',
      'Prénom': 'Cyrille',
      'Email': 'gaucher.cyrille@mail.com',
      'Entité': 'Dev Insight - Entreprise',
      'Rôle': 'Administrator',
      'Statut': 'inactive',
      'Actions': ['edit', 'delete', 'connection']
    }
  ]

  // Entreprises Component
  entreprisesColumns = [
    'Nom',
    'Display Name',
    'Nombre d\'établissements',
    'Actions'
  ]
  entreprises = [
    {
      'Nom': 'Dev Insight',
      'Display Name': 'dev-insight',
      'Nombre d\'établissements': '1',
      'Actions': ['edit']
    },
    {
      'Nom': 'We Recruit',
      'Display Name': 'we-recruit',
      'Nombre d\'établissements': '9',
      'Actions': ['edit']
    }
  ]

  // Lost animation 'cause it doesn't activate on component loading, need to get it back somehow
  holdingUrl(holding){
    console.log(this.expandedElement);
    if(this.holdingClickCount < 1){
      this.router.navigate([`/application/clients/${holding}`]);
      this.holdingClickCount++;
    }else if(this.expandedElement || this.expandedElement===null){
      this.router.navigate([`/application/clients/${holding}`]);
      this.holdingClickCount++;
    }else{
      this.router.navigate([`/application/clients`]);
      this.holdingClickCount = 0;
    }
    
  }
  constructor(private router:Router) { }

  ngOnInit() {
    for(let data of this.dataSource){
      if(data.Holding === this.holdingName){
        this.expandedElement = data;
        this.holdingClickCount++;
      }
    }
  }

}
