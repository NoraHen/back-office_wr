import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';
import { MatTableModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { MainComponent } from './main/main.component';
import { ClientsComponent } from './main/application/clients/clients.component';
import { UtilisateursComponent } from './main/application/utilisateurs/utilisateurs.component';
import { OffresComponent } from './main/application/offres/offres.component';
import { LogsComponent } from './main/application/logs/logs.component';
import { EngagementClientComponent } from './main/application/engagement-client/engagement-client.component';
import { DemandesComponent } from './main/site/demandes/demandes.component';
import { PagesComponent } from './main/site/pages/pages.component';
import { BlogComponent } from './main/site/blog/blog.component';
import { GalerieComponent } from './main/site/galerie/galerie.component';
import { ComptesComponent } from './main/utilisateurs/comptes/comptes.component';
import { FormHeaderComponent } from './components/form-header/form-header.component';
import { FormTableComponent } from './components/form-table/form-table.component';

const appRoutes: Routes = [
    {
        path: 'application', component: MainComponent
    },
    {
        path: 'application/clients', component: MainComponent
    },
    {
        path: 'application/clients/:holdingName', component: MainComponent
    },
    {
        path: 'application/utilisateurs', component: MainComponent
    },
    {
        path: 'application/offres', component: MainComponent
    },
    {
        path: 'application/logs', component: MainComponent
    },
    {
        path: 'application/engagement-client', component: MainComponent
    },
    {
        path: 'site', component: MainComponent
    },
    {
        path: 'site/demandes', component: MainComponent
    },
    {
        path: 'site/pages', component: MainComponent
    },
    {
        path: 'site/blog', component: MainComponent
    },
    {
        path: 'site/galerie', component: MainComponent
    },
    {
        path: 'utilisateurs', component: MainComponent
    },
    {
        path: 'utilisateurs/comptes', component: MainComponent
    }
];

@NgModule({
    declarations: [
        AppComponent,
        // Currently trying to make a module out of it
        MainComponent,
        ClientsComponent,
        UtilisateursComponent,
        OffresComponent,
        LogsComponent,
        EngagementClientComponent,
        DemandesComponent,
        PagesComponent,
        BlogComponent,
        GalerieComponent,
        ComptesComponent,
        FormHeaderComponent,
        FormTableComponent,
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),


        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatTableModule,
        CdkTableModule,
        MatTabsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatDatepickerModule,
        MatRadioModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        // MainModule
    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
