import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'site',
        title    : 'Site',
        // How does his translation system work?
        // Do I even need to care about it?
        // translate: 'NAV.SITE',
        type     : 'group',
        children : [
            {
                id       : 'demandes',
                title    : 'Demandes',
                // translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'email',
                url      : '/site/demandes',
                // badge    : {
                //     title    : '25',
                //     translate: 'NAV.SAMPLE.BADGE',
                //     bg       : '#F44336',
                //     fg       : '#FFFFFF'
                // }
            },
            {
                id       : 'page',
                title    : 'Pages',
                type     : 'item',
                icon     : 'pages',
                url      : '/site/pages',
            },
            {
                id       : 'blog',
                title    : 'Blog',
                type     : 'item',
                icon     : 'create',
                url      : '/site/blog',
            },
            {
                id       : 'galerie',
                title    : 'Galerie',
                type     : 'item',
                icon     : 'image',
                url      : '/site/galerie',
            }
        ]
    },
    {
        id       : 'application',
        title    : 'Application',
        type     : 'group',
        children : [
            {
                id       : 'clients',
                title    : 'Clients',
                type     : 'item',
                icon     : 'business',
                url      : 'application/clients',
            },
            {
                id       : 'utilisateurs',
                title    : 'Utilisateurs',
                type     : 'item',
                icon     : 'contacts',
                url      : 'application/utilisateurs',
            },
            {
                id       : 'offres',
                title    : 'Offres',
                type     : 'item',
                icon     : 'create',
                url      : 'application/offres',
            },
            {
                id       : 'logs',
                title    : 'Logs',
                type     : 'item',
                icon     : 'library_books',
                url      : 'application/logs',
            },
            {
                id       : 'engagement-client',
                title    : 'Engagement Client',
                type     : 'item',
                icon     : 'library_books',
                url      : 'application/engagement-client',
            }
        ]
    },
    {
        id       : 'utilisateurs',
        title    : 'Utilisateurs',
        type     : 'group',
        children : [
            {
                id       : 'comptes',
                title    : 'Comptes',
                type     : 'item',
                icon     : 'account_circle',
                url      : 'utilisateurs/comptes',
            }
        ]
    }
];
