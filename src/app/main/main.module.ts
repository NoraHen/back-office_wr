import { NgModule, Input } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { CardedFullWidthTabbed2Component } from 'app/layout/page-layout/full-width-tabbed-2/full-width-tabbed-2.component';
// import { DemandesModule } from '../../../main/site/demandes/demandes.module';
import { MainComponent } from './main.component';

@NgModule({
    declarations: [
        CardedFullWidthTabbed2Component,
        // DemandesModule
    ],
    imports     : [
        RouterModule,
        CommonModule,
        MainComponent,
        CardedFullWidthTabbed2Component,
        
    ],
    exports     : [
        MainComponent
    ]
})

export class MainModule
{
}
