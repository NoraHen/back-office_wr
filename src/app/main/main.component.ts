import { Component, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  // Route Params
  holdingName: any;
  categoryTitle: string
  
  @ViewChild('pageContainer', {read: ViewContainerRef, static:true}) pageContainer: ViewContainerRef

  @ViewChild('demandes', {static: true}) demandesTemplate: TemplateRef<any>
  @ViewChild('pages', {static: true}) pagesTemplate: TemplateRef<any>
  @ViewChild('blog', {static: true}) blogTemplate: TemplateRef<any>
  @ViewChild('galerie', {static: true}) galerieTemplate: TemplateRef<any>
  @ViewChild('clients', {static: true}) clientsTemplate: TemplateRef<any>
  @ViewChild('utilisateurs', {static: true}) utilisateursTemplate: TemplateRef<any>
  @ViewChild('offres', {static: true}) offresTemplate: TemplateRef<any>
  @ViewChild('logs', {static: true}) logsTemplate: TemplateRef<any>
  @ViewChild('engagementClient', {static: true}) engagementClientTemplate: TemplateRef<any>
  @ViewChild('comptes', {static: true}) comptesTemplate: TemplateRef<any>

  // pagesTemplates = {
  //   demandes: this.demandesTemplate,
  //   pages: this.pagesTemplate,
  //   blog: this.blogTemplate,
  //   galerie: this.galerieTemplate,
  //   [Symbol.iterator]: function* (){
  //     let properties = Object.keys(this);
  //     for (let property of properties){
  //       yield [property, this[property]];
  //     }
  //   }
  // }

  constructor(private route:ActivatedRoute) { }

  ngOnChanges() {

  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params)=>{
          this.holdingName = params['holdingName']
        }
      )
    let url = this.route.snapshot.url

    this.categoryTitle = `${url[1].path[0].toUpperCase()}${url[1].path.substring(1)}`

    // Could just ngIf or ngSwitchCase the whole thing instead of using templates...
    // Gosh I wanna find a way to bind TemplateRef to an iterable
    switch (url[1].path) {
      case 'demandes':
        this.pageContainer.createEmbeddedView(this.demandesTemplate)
        break;
      case 'pages':
        this.pageContainer.createEmbeddedView(this.pagesTemplate)
        break;
      case 'blog':
        this.pageContainer.createEmbeddedView(this.blogTemplate)
        break;
      case 'galerie':
        this.pageContainer.createEmbeddedView(this.galerieTemplate)
        break;
      case 'clients':
        this.pageContainer.createEmbeddedView(this.clientsTemplate)
        break;
      case 'utilisateurs':
        this.pageContainer.createEmbeddedView(this.utilisateursTemplate)
        break;
      case 'offres':
        this.pageContainer.createEmbeddedView(this.offresTemplate)
        break;
      case 'logs':
        this.pageContainer.createEmbeddedView(this.logsTemplate)
        break;
      case 'engagement-client':
        this.pageContainer.createEmbeddedView(this.engagementClientTemplate)
        break;
      case 'comptes':
        this.pageContainer.createEmbeddedView(this.comptesTemplate)
        break;
      default:
        console.log('Woops, not a current url!');
    }
    // for (let pageTemplate of this.pagesTemplates){
    //   if(url[1].path === pageTemplate[0]){
        // So far I will need to make a switch,
        // I can't seem to store the template ref within an iterable,
        // it becomes undefined right away
        // Why?
        // console.log(pageTemplate[1])
        // this.pageContainer.createEmbeddedView(pageTemplate[1])
    //   }
    // }


  }

}
