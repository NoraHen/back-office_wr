import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  @Input() holdingName: string;

  columnsToDisplay = [
    'Holding',
    'Nombre d\'entreprises',
    'Création',
    'Statut',
    'Département',
    'Pays',
    'Client',
    'Contrat',
    'Début',
    'Fin',
    'Utilisateurs',
    'Offres Actives',
    'Fonctionnalités',
    'CSM',
    'Sales',
    'Actions'
    // 'Secteur',
    //   'Taille',
  ]

  dataSource = [
    {
      'Holding': 'Asus WR',
      'Nombre d\'entreprises': '1',
      'Création': '27/03/2019',
      'Statut': 'radio_button_checked',
      'Département': '35',
      'Pays': 'France',
      'Client': 'Hervé Sarah',
      'Contrat': 'Annuel',
      'Début': '27/03/2019',
      'Fin': '27/03/2020',
      'Utilisateurs': '1',
      'Offres Actives': '1',
      'Fonctionnalités': '2',
      'CSM': 'Lisa',
      'Sales': 'Maëlle',
      'Actions': ['delete'],
      'Secteur': 'Industrie Manufacturière et Production',
      'Taille de l\'entreprise': '0 - 10 salariés',
      'Origine du Contact': 'Site internet',
      'Contact': {
        'Nom': 'Hervé',
        'Prénom': 'Sarah',
        'Email': 'herve.sarah@mail.com',
        'Raison Sociale': 'Asus WR',
        'Adresse': '6 rue d\'Ouessant',
        'Code Postal': '35760',
        'Ville': 'Saint-Grégoire',
        'Pays': 'France'
      }
    },
    {
      'Holding': 'DevInsight',
      'Nombre d\'entreprises': '1',
      'Création': '08/07/2019',
      'Statut': 'remove_circle',
      'Département': '35',
      'Pays': 'France',
      'Client': 'Goux Pierre',
      'Contrat': 'Crédits',
      'Début': '08/07/2019',
      'Fin': '08/10/2019',
      'Utilisateurs': '1',
      'Offres Actives': '2',
      'Fonctionnalités': '3',
      'CSM': 'Marine',
      'Sales': 'Mélanie',
      'Actions': ['delete'],
      'Secteur': 'Ingénierie - Bureau d\'études',
      'Taille de l\'entreprise': '0 - 10 salariés',
      'Origine du Contact': 'Site internet',
      'Contact': {
        'Nom': 'Goux',
        'Prénom': 'Pierre',
        'Email': 'goux.pierre@mail.com',
        'Raison Sociale': 'Dev Insight',
        'Adresse': '6 rue des paquerettes',
        'Code Postal': '35000',
        'Ville': 'Rennes',
        'Pays': 'France'
      }
    },
    {
      'Holding': 'We Recruit',
      'Nombre d\'entreprises': '9',
      'Création': '25/01/2016',
      'Statut': 'radio_button_unchecked',
      'Département': '35',
      'Pays': 'France',
      'Client': 'Panaget François-Xavier',
      'Contrat': 'Démo',
      'Début': '25/01/2016',
      'Fin': '99/99/9999',
      'Utilisateurs': '28',
      'Offres Actives': '15',
      'Fonctionnalités': '33',
      'CSM': 'Marine',
      'Sales': 'Maëlle',
      'Actions': ['delete'],
      'Secteur': 'Recrutement - Conseil RH - Interim',
      'Taille de l\'entreprise': '11 - 20 salariés',
      'Origine du Contact': 'Site internet',
      'Contact': {
        'Nom': 'Panaget',
        'Prénom': 'François-Xavier',
        'Email': 'fx.panaget@mail.com',
        'Raison Sociale': 'We Recruit',
        'Adresse': '6 rue d\'Ouessant',
        'Code Postal': '35760',
        'Ville': 'Saint-Grégoire',
        'Pays': 'France'
      }
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
