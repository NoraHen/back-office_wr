import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngagementClientComponent } from './engagement-client.component';

describe('EngagementClientComponent', () => {
  let component: EngagementClientComponent;
  let fixture: ComponentFixture<EngagementClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngagementClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngagementClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
